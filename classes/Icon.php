<?php
class icon
{
  // Properties
  public $url;
  public $path;
  public $w;
  public $h;
  public $aspect;
  public $link;

  public function __construct($path, $url, $link = null) {
    $this->path = $path;
    $this->w = getimagesize($this->path)[0];
    $this->h = getimagesize($this->path)[1];
    $this->aspect = $this->w / $this->h;
    $this->link = $link;
    $this->url = $url;
  }

  public function display($width) {
    if($this->w < $width) {
      echo "Requested logo size greater then uploaded file.";
    } else {
      $height = $width / $this->aspect;
      if($this->link) {
        echo '<a href="' . $this->link . '"><img src="' . $this->url . $this->path . '" width="'. $width .'" height="'. intval($height) .'"/></a>';
      } else {
        echo '<img src="' . $this->url . $this->path . '" width="'. $width .'" height="'. intval($height) .'"/>';
      }
    }
  }
}
