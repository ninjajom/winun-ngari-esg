<?php
class logo
{
  // Properties
  public $url;
  public $path;
  public $w;
  public $h;
  public $aspect;
  public $name;

  public function __construct($path, $url = '', $name = null) {
    $this->path = $path;
    $this->w = getimagesize($this->path)[0];
    $this->h = getimagesize($this->path)[1];
    $this->aspect = $this->w / $this->h;
    $this->url = $url;
    $this->name = $name;
  }

  public function name() {
    echo $this->name;
  }

  public function url() {
    echo $this->url;
  }

  public function display($width) {
    if($this->w < $width) {
      echo "Requested logo size greater then uploaded file.";
    } else {
      $height = $width / $this->aspect;
      echo '<img src="' . $this->url . $this->path . '" width="'. $width .'" height="'. intval($height) .'"/>';
    }
  }
}
