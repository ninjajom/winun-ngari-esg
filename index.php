<?php
	include_once "variables.php";
?>
<!-- // This file is for the display of the GUI for the generator only. Styles here should be separate from the signature itself. -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?= $pageTitle ?></title>
	<link rel="stylesheet" href="node_modules/bulma/css/bulma.min.css">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-j8y0ITrvFafF4EkV1mPW0BKm6dp3c+J9Fky22Man50Ofxo2wNe5pT1oZejDH9/Dt" crossorigin="anonymous">
</head>
<style>
	.is-hmt {
		background-color: #ee3640;
	}
</style>
<body>
	<section class="hero is-small is-hmt">
		<div class="hero-body">
			<h1 class="title has-text-white"><?= $pageTitle ?></h1>
		</div>
	</section>

	<section class="section">
		<div id="userInput" class="container">
			<div class="field">
				<label class="label">Name</label>
				<div class="control">
					<input type="text" class="input" id="name" name="name" placeholder="First & Last Name" value="" onblur="updateOutput()" onkeyup="updateEmail()">
				</div>
			</div>
			<div class="field">
				<label class="label">Job</label>
				<div class="control">
					<input type="text" class="input" id="job" name="job" placeholder="Job Title" value="" onblur="updateOutput()" onkeyup="updateEmail()">
				</div>
			</div>
			<!-- <div class="field">
				<label class="label">Work Area</label>
				<div class="control">
					<input type="text" class="input" id="area" name="area" placeholder="Area of Work" value="" onblur="updateOutput()" onkeyup="updateEmail()">
				</div>
			</div> -->
			<div class="field">
				<label class="label">Address</label>
				<div class="control">
					<div class="select">
						<select id="address" onchange="setAddress()">
							<option value="None">None</option>
							<?php foreach($addresses as $address) { ?>
								<option class="dropdown-item" value="<?= $address->printAddressInline() ?>"><?= $address->printAddressInline() ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<span class="address is-hidden" id="addressBtn">0</span>
			</div>

			<!-- Images -->
			<div class="field">
				<label class="label">Select Image</label>
				<div class="control">
					<div class="select">
						<select id="image" onchange="setImage()">
							<?php foreach($images as $key => $image) { ?>
								<option class="dropdown-item" value="<?= $key ?>"><?= $image->name() ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<span class="address is-hidden" id="imageBtn">0</span>
			</div>

			<label class="label">Email</label>
			<div class="field has-addons">
				<div class="control">
					<input type="text" class="input" id="email" name="email" placeholder="firstname" value="" aria-describedby="emailAddon" onblur="updateOutput()">
				</div>
				<div class="control">
					<a id="emailAddon" class="button is-static">
						<?php echo "@" . $URL; ?>
					</a>
				</div>
			</div>

			<!-- <div class="field">
				<label class="label">Phone</label>
				<div class="control">
					<input type="phone" class="input" id="phone" name="phone" placeholder="<?= $primaryPhone; ?>" value="" onblur="updateOutput()">
				</div>
			</div> -->

			<div class="field">
				<label class="label">Mobile</label>
				<div class="control">
					<input type="mobile" class="input" id="mobile" name="mobile" placeholder="0400 000 000" value="" onblur="updateOutput()">
				</div>
		</div>
	</section>
	<section class="section">
		<div class="container">
			<div id="output"></div>
			<br/>
			<div class="field is-grouped is-grouped-centered">
				<div class="control">
					<button class="button is-primary" onclick="downloadOutput()"><i class="fa fa-download"></i>&nbsp;&nbsp;Download</button>
				</div>
				<div class="control">
					<button class="button is-info" id="viewInstructions"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Instructions</button>
				</div>
			</div>
		</div>
	</section>

	<!-- Instructions modal  -->
	<div id="modalInstructions" class="modal">
		<div class="modal-background"></div>
		<div class="modal-card">
			<header class="modal-card-head">
				<p class="modal-card-title">Email Signature Generator Instructions</p>
				<button class="delete" onclick="closeModal()" aria-label="close"></button>
			</header>
			<section class="modal-card-body">
				<div id="instructionsDynamic"></div>
			</section>
			<footer class="modal-card-foot">
				<button type="button" onclick="closeModal()" class="button is-secondary">Close</button>
			</footer>
		</div>
	</div>

	<script src="node_modules/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</html>
