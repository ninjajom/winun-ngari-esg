<?php
  include_once "functions.php";
  $pageTitle = "Winun Ngari Email Signature Generator";

  // Organisation details
  $businessName = "Winun Ngari Aboriginal Corporation";
  $businessTagline = "";
  $primaryPhone = '0424 968 900';
  $URL = "winunngari.org.au";
  $disclaimer["title"] = "Private and confidential";
  $disclaimer["content"] = "This message is confidential. It may also be privileged or otherwise protected by work product immunity or other legal rules. If you have received it by mistake, please let us know by e-mail reply and delete it from your system; you may not copy this message or disclose its contents to anyone. Please send us by fax any message containing deadlines as incoming e-mails are not screened for response deadlines. The integrity and security of this message cannot be guaranteed on the Internet.";

  include_once "classes/Address.php";
  $addresses = array();
  $addresses[0] = new Address();
  $addresses[0]->locationName = "";
  $addresses[0]->line1 = "145 Loch St" . " ";
  $addresses[0]->suburb = "Derby" . " ";
  $addresses[0]->state = "WA" . " ";
  $addresses[0]->postcode = "6728" . " ";

  $addresses[1] = new Address();
  $addresses[1]->locationName = "";
  $addresses[1]->line1 = "48 Clarendon St" . " ";
  $addresses[1]->suburb = "Derby" . " ";
  $addresses[1]->state = "WA" . " ";
  $addresses[1]->postcode = "6725" . " ";

  $addresses[2] = new Address();
  $addresses[2]->locationName = "";
  $addresses[2]->line1 = "45 Clarendon St" . " ";
  $addresses[2]->suburb = "Derby" . " ";
  $addresses[2]->state = "WA" . " ";
  $addresses[2]->postcode = "6725" . " ";

  /*
  If there are more addesses you can repeat this code by adding another address object with the next key inline. Example: "$addresses[1] becomes $addresses[2]"

  $addresses[1] = new Address();
  $addresses[1]->name = "Street Address";
  $addresses[1]->streetNo = "9";
  $addresses[1]->streetName = "Lucas";
  $addresses[1]->streetType = "Street";
  $addresses[1]->suburb = "Broome";
  $addresses[1]->state = "WA";
  $addresses[1]->postcode = "6725";

  */

  // Styling details
  $fontStack = "'Raleway', 'HelveticaNeue', 'Lucida Sans Unicode', Tahoma, Sans-Serif";
  $signatureWidth = "600";
  $primaryColor = "black";
  $secondaryColor = "white";
  $highlightColor = "#FFE63F";
  $mutedColor = "#808285";
  $assetDirectory = "assets/";

  // Styles
  $styHead1 = "font-family: $fontStack;
    font-size: 22px;
    font-weight: 700;
    text-transform: uppercase;
    color: $primaryColor;"
  ;

  $styHead2 = "font-family: $fontStack;
    font-size: 14px;
    font-weight: 400;
    color: $primaryColor"
  ;
  $styHead3 = "font-family: $fontStack;
    font-size: 15px;
    font-weight: 700;
    text-transform:uppercase;
    color: $primaryColor"
  ;

  $styHead4 = "font-family: $fontStack;
    font-size: 12px;
    font-weight: bold;
    text-transform: uppercase;
    color: $secondaryColor;"
  ;

  $styHead5 = "font-family: $fontStack;
    font-size: 14px;
    font-weight: 800;
    color: white;
    margin-right: 15px;
    margin-bottom: 5px;
    text-align: right;"
  ;

  $styHead6 = "font-family: $fontStack;
    font-size: 14px;
    font-weight: 700;
    text-transform: uppercase;
    color: $mutedColor;"
  ;

  $styVertLine = "border-right: 2px solid #ce1f43;
    padding:30px;"
  ;

  $styFooter = "background-color: #000000;
    text-align: right;
    padding-bottom:10px;
    padding-right:10px;"
  ;

  $styBottomImage = "background-color: #000000;"
  ;

  $styTagline = "font-family: $fontStack;
    font-weight: 700;
    color: $primaryColor;
    text-align: right;
    text-transform: uppercase;
    font-size: 13px;"
  ;

  $styBody = "font-family: $fontStack;
    font-weight: 400;
    color: $primaryColor;
    font-size: 13px;
    display: inline;"
  ;

  $stySocial = "margin-left: 70px;"
  ;

  $styDisclaimer = "font-family: $fontStack;
    font-weight: 100;
    font-size: 10px;
    text-align: justify;
    text-justify: inter-word;
    "
  ;

  $styWebsite = "font-family: $fontStack;
    color: $primaryColor;"
  ;

  $thickLine = "border-bottom: 6px solid #ce1f43;";

  // Generate asset URL
  if($_SERVER["HTTP_HOST"] == 'localhost') {
    $assetURL = "http://localhost:8000/";
  } else {
    $assetURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/";
  }

  // Define images
  include_once "classes/Logo.php";
  //$logo["primary"] = new Logo($assetDirectory . "logo2.png", $assetURL);
  $logo[0] = new Logo($assetDirectory . "corporate-logo.png", $assetURL, "Corporate Services");
  $logo[1] = new Logo($assetDirectory . "employment-logo.png", $assetURL, "Employment Services");
  $logo[2] = new Logo($assetDirectory . "rsas-logo.png", $assetURL, "Remote School Attendance Strategy");
  $logo[3] = new Logo($assetDirectory . "money-logo.png", $assetURL, "Money Management");

  // // Define Icons
  // include_once "classes/Icon.php";
  // $icon["instagram"] = new Icon($assetDirectory . "instagram.png", $assetURL, "https://www.instagram.com/yurraptyltd/");
  // $icon["facebook"] = new Icon($assetDirectory . "facebook.png", $assetURL, "https://www.facebook.com/yurraptyltd/");

  // Define images
  $images[0] = new Logo($assetDirectory . "corporate.png", $assetURL, "Corporate Services");
  $images[1] = new Logo($assetDirectory . "employment2.png", $assetURL, "Employment Services");
  $images[2] = new Logo($assetDirectory . "RSAS.png", $assetURL, "Remote School Attendance Strategy");
  $images[3] = new Logo($assetDirectory . "money.png", $assetURL, "Money Management");

?>
