function updateOutput()
{
  var inputName = $("#name").val();
  var inputAddress = $("#addressBtn").text();
  if($("#email").val() != "")
  {
    var inputEmail = $("#email").val().trim() + $("#emailAddon").text().trim();
  } else {
    var inputEmail = "";
  }
  if($("#job").val() != "")
  {
    var inputJob = $("#job").val();
  } else {
    var inputJob = "";
  }
  if($("#area").val() != "")
  {
    var inputArea = $("#area").val();
  } else {
    var inputArea = "";
  }
  if($("#phone").val() != "")
  {
    var inputPhone = $("#phone").val();
  } else {
    var inputPhone = "";
  }
  var inputMobile = $("#mobile").val();
  if($("#facebook").val() != "")
  {
    var inputFacebook = $("#facebookAddon").html() + $("#facebook").val();
  } else {
    var inputFacebook = "";
  }
  if($("#instagram").val() != "")
  {
    var inputInstagram = $("#instagramAddon").html() + $("#instagram").val();
  } else {
    var inputInstagram = "";
  }
  if($("#linkedin").val() != "")
  {
    var inputLinkedIn = $("#linkedinAddon").html() + $("#linkedin").val();
  } else {
    var inputLinkedIn = "";
  }
  var inputImage = $("#imageBtn").text();
  var outputResult;

  $.ajax({
    url: "signature-template.php",
    method: "POST",
    data: {
      name: inputName, 
      job: inputJob, 
      area: inputArea, 
      address: inputAddress, 
      phone:inputPhone, 
      mobile: inputMobile, 
      email: inputEmail, 
      facebook: inputFacebook, 
      instagram: inputInstagram, 
      linkedin: inputLinkedIn,
      image: inputImage
    },
    success: function(result){
        $("#output").html(result);
        outputResult = result;
    }});
}

function updateEmail()
{
  var inputName = $("#name").val();

  var firstName = inputName.split(" ");

  $("#email").val(firstName['0'].toString().toLowerCase());
}

function setAddress()
{
  var address = $("#address").val();
  if(address !== 'None') {
    $("#addressBtn").text(address);
  } else {
    $("#addressBtn").text('');
  }

  updateOutput();
}

function setImage()
{
  var image = $("#image").val();
  if(image !== 'None') {
    $("#imageBtn").text(image);
  } else {
    $("#imageBtn").text('');
  }

  updateOutput();
}

function downloadOutput()
{
  updateOutput();

  var result = $("#output").html();
  var inputName = $("#name").val();

  // Create a form
  var downloadForm = document.createElement("form");
  downloadForm.style.display = 'none';
  downloadForm.target = "_blank";
  downloadForm.method = "POST";
  downloadForm.action = "download.php";

  // Create an input
  var formOutfile = document.createElement("input");
  formOutfile.type = "text";
  formOutfile.name = "outfile";
  formOutfile.value = result;

  // Create an input
  var formName = document.createElement("input");
  formName.type = "text";
  formName.name = "name";
  formName.value = inputName;

  // Add the input to the form
  downloadForm.appendChild(formOutfile);
  downloadForm.appendChild(formName);

  // Add the form to dom
  document.body.appendChild(downloadForm);

  // Just submit
  downloadForm.submit();
}

updateOutput();

$("#viewInstructions").click(function() {
  $("#instructionsDynamic" ).load( "https://instructions.esg.beluca.net/" );
  $("#modalInstructions").addClass("is-active");
});

var closeModal = function() {
  $("#modalInstructions").removeClass("is-active");
}
